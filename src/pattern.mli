(** A pattern is a motif of British pub name. *)
type pattern =
  | Possessive
      (** The possessive pub name is of the form "The X's Y": The
          Queen's head, The rabbit's foot, The genie's lamp, The
          gardner's shovel, The goose's egg, etc.

          It can be replaced by [Free "The $(NOUN)'s $(NOUN)"] but this
          will not apply the "s's" contraction. Meaning it might
          generate "The witches's brew". Use [Possessive] to avoid
          this. *)
  | Anded
      (** The anded pub name is of the form "The X and Y": The cow and
          horse, The cheese and mouse, The shoe and goat, The buck and hog,
          etc.

          It can be replaced by [Free "The $(NOUN) and $(NOUN)"] but this
          will not carefully pick the order of the nouns. Meaning it might
          generate "The groom and bride" or "The brook and mill". Use
          [Anded] so that the nouns are sorted vaguely by
          tic-tac-toe/big-bad-wolf sound order. *)
  | Free of Bos.Pat.t
      (** The free pattern pub name is of the form specified by
          the pattern. The pattern may use any of the variables
          from the domain of [Variables.vars]. *)

(** [qualified] is [Free "The $(QUALIFIER) $(NOUN)"] *)
val qualified : pattern

(** [numbered] is [Free "The $(CARDINAL) $(NOUN_PLURAL)"].
    Note that the numbers in [CARDINAL] are never [one], hence the plural. *)
val numbered : pattern

(** [default] is a reasonable set of patterns to chose from in case the user has
    not specified one. *)
val default : pattern Pick.choice

(** [run p] prints a pub name of the specified pattern on [/dev/stdout]. *)
val run : pattern -> unit
