open Astring

(* HELPERS *)

let capitalise s =
  let s = Bytes.of_string s in
  Bytes.set s 0 (Char.Ascii.uppercase (Bytes.get s 0));
  Bytes.unsafe_to_string s
;;

type pattern =
  | Possessive
  | Anded
  | Free of Bos.Pat.t

let qualified = Free (Result.get_ok (Bos.Pat.of_string "The $(QUALIFIER) $(NOUN)"))
let numbered = Free (Result.get_ok (Bos.Pat.of_string "The $(CARDINAL) $(NOUN_PLURAL)"))
let default = Pick.arr [| Possessive; Anded; qualified; numbered |]

let run = function
  | Possessive ->
    let n1 = capitalise (Pick.pick Variables.nouns) in
    let n2 = Pick.pick Variables.nouns in
    let possessive = if n1.[String.length n1 - 1] <> 's' then "s" else "" in
    Printf.printf "The %s'%s %s\n" n1 possessive n2
  | Anded ->
    let n1, n2 = Pick.pick2 Variables.nouns in
    Printf.printf "The %s and %s\n" n1 n2
  | Free p ->
    let s =
      Bos.Pat.format
        ~undef:(fun k ->
          match String.Map.find_opt k Variables.vars with
          | Some choices -> Pick.pick choices
          | None -> failwith (Format.asprintf "Invalid VAR in PATTERN: %S" k))
        String.Map.empty
        p
    in
    Printf.printf "%s\n" s
;;
