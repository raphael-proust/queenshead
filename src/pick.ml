type 'a choice =
  | Leaves of 'a array
  | Node of
      { total_length : int
      ; choices : 'a choice array
      }

let length = function
  | Leaves a -> Array.length a
  | Node { total_length; choices = _ } -> total_length
;;

let arr es = Leaves es

let choices choices =
  let total_length = Array.fold_left (fun acc c -> acc + length c) 0 choices in
  Node { total_length; choices }
;;

let rec nth choice n =
  match choice with
  | Leaves a -> Array.get a n
  | Node { total_length = _; choices } ->
    let rec loop i n =
      let choice_i = Array.get choices i in
      let l = length choice_i in
      if l <= n then loop (i + 1) (n - l) else nth choice_i n
    in
    loop 0 n
;;

let pick choice = nth choice (Random.int (length choice))

let pick2 choice =
  (* preserve order *)
  let l = length choice in
  let a = Random.int l in
  let b = Random.int l in
  let i = min a b in
  let j = max a b in
  nth choice i, nth choice j
;;
